/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.killersmurf.antipvplogger;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 *
 * @author Dylan
 */
public class CombatTask implements Runnable {

    private Player player;
    private AntiPvPLogger al;
    private Player attacker;

    public CombatTask(Player player, AntiPvPLogger al, Player attacker) {
        this.player = player;
        this.al = al;
        this.attacker = attacker;
    }

    public void run() {
        attacker.sendMessage(ChatColor.RED + "You're no longer in combat!");
        al.setInCombat(attacker.getName(), Boolean.FALSE);
        al.setTask(attacker.getName(), -1);
        al.setInCombat(player.getName(), Boolean.FALSE);
        al.setTask(player.getName(), -1);
        player.sendMessage(ChatColor.RED + "You're no longer in combat!");
    }
}

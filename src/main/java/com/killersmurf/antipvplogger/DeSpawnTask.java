/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.killersmurf.antipvplogger;

import com.topcat.npclib.NPCManager;

/**
 *
 * @author Dylan
 */
public class DeSpawnTask implements Runnable {

    private String npcName;
    private NPCManager nm;

    public DeSpawnTask(String npcName, NPCManager nm) {
        this.npcName = npcName;
        this.nm = nm;
    }

    public void run() {
        nm.despawnHumanByName(npcName);
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.killersmurf.antipvplogger.listeners;

import com.killersmurf.antipvplogger.AntiPvPLogger;
import com.killersmurf.antipvplogger.CombatTask;
import com.massivecraft.factions.*;
import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.topcat.npclib.entity.HumanNPC;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.PluginManager;

/**
 *
 * @author Dylan
 */
public class ALListener implements Listener {

    private AntiPvPLogger al;

    public ALListener(AntiPvPLogger al) {
        this.al = al;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        if (player.hasPermission("antipvplogger.bypass") || player.getGameMode().getValue() == 1) {
            return;
        }

        String name = player.getName();
        if (Bukkit.getPluginManager().isPluginEnabled("WorldGuard")) {
            WorldGuardPlugin worldGuard = (WorldGuardPlugin) Bukkit.getPluginManager().getPlugin("WorldGuard");
            LocalPlayer localPlayer = worldGuard.wrapPlayer(player);
            if (!worldGuard.getRegionManager(player.getWorld()).getApplicableRegions(player.getLocation()).allows(DefaultFlag.PVP, localPlayer)) {
                return;
            }
        }
        if (Bukkit.getPluginManager().isPluginEnabled("Factions")) {
            P p = (P) Bukkit.getPluginManager().getPlugin("Factions");
            FPlayer fPlayer = FPlayers.i.get(player);
            Faction defFaction = Board.getFactionAt(new FLocation(fPlayer.getPlayer().getLocation()));
            if (defFaction.isSafeZone()) {
                return;
            }
        }
        if (!al.inCombat(name) && !al.playersNearby(player)) {
            return;
        }
        al.setInCombat(name, Boolean.FALSE);
        HumanNPC npc = (HumanNPC) al.spawnHumanNPC(player, player.getLocation(), name);

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event) {
        final Player player = event.getPlayer();
        if (player.hasPermission("antipvplogger.bypass")) {
            return;
        }
        Bukkit.getScheduler().scheduleSyncDelayedTask(al, new Runnable() {

            public void run() {
                String name = player.getName();
                Bukkit.getScheduler().cancelTask(al.getTask(name));
                if (!al.isDead(name)) {
                    al.nm.despawnHumanByName(name);
                    return;
                }
                al.removeDead(name);
                player.getInventory().clear();
                player.getInventory().setArmorContents(null);
                player.setHealth(20);
                player.setSaturation(10);
                player.setExp(0);
                player.teleport(player.getWorld().getSpawnLocation());
                player.sendMessage(ChatColor.RED + "Your NPC was killed while Combat Logged!");
            }
        }, 2L);

    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerDeath(PlayerDeathEvent event) {
        if (!al.nm.isNPC(event.getEntity())) {
            return;
        }
        HumanNPC npc = al.nm.getOneHumanNPCByName(event.getEntity().getName());
        al.addDead(npc.getName());
        Bukkit.broadcastMessage(ChatColor.RED + npc.getName() + "'s" + ChatColor.YELLOW + " NPC has been killed while combat logged!");
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityDamageByEntity(EntityDamageEvent event) {
        if (event.isCancelled()) {
            return;
        }

        if (!(event instanceof EntityDamageByEntityEvent)) {
            return;
        }
        EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;
        if (e.isCancelled()) {
            return;
        }
        if (!(e.getDamager() instanceof Player)) {
            return;
        }

        if (!al.nm.isNPC(event.getEntity()) && event.getEntity() instanceof Player) {
            Player attacker = (Player) e.getDamager();
            Player player = (Player) e.getEntity();
            al.setInCombat(attacker.getName(), Boolean.TRUE);
            al.setInCombat(player.getName(), Boolean.TRUE);
            Integer prevTaskId = al.getTask(player.getName());
            Integer prevTaskIdAttacker = al.getTask(attacker.getName());

            if (prevTaskId != null || prevTaskId != -1) {
                Bukkit.getScheduler().cancelTask(prevTaskId);
            }

            if (prevTaskId == null || prevTaskId == -1) {
                player.sendMessage(ChatColor.RED + "You're now in combat!");
            }
            if (prevTaskIdAttacker == null || prevTaskIdAttacker == -1) {
                attacker.sendMessage(ChatColor.RED + "You're now in combat!");
            }

            int taskId = Bukkit.getScheduler().scheduleSyncDelayedTask(al, new CombatTask(player, al, attacker), 300L);
            al.setTask(player.getName(), taskId);
            al.setTask(attacker.getName(), taskId);
        } else if (al.nm.isNPC(event.getEntity())) {
            Player player = (Player) e.getDamager();
            al.setInCombat(player.getName(), Boolean.TRUE);
            HumanNPC npc = al.nm.getOneHumanNPCByName(player.getName());
        }
    }

    public void registerEvents() {
        final PluginManager pm = al.getServer().getPluginManager();
        pm.registerEvents(this, al);
    }
}

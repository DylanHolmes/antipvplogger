package com.killersmurf.antipvplogger;

import com.killersmurf.antipvplogger.listeners.ALListener;
import com.topcat.npclib.NPCManager;
import com.topcat.npclib.entity.HumanNPC;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author Dylan_Holmes
 *
 */
public class AntiPvPLogger extends JavaPlugin {

    public static final Logger LOGGER = Logger.getLogger("Minecraft");
    private List<String> deadPlayers = new ArrayList<String>();
    private Map<String, Boolean> combatMap = new HashMap<String, Boolean>();
    private Map<String, Integer> taskMap = new HashMap<String, Integer>();
    private Integer time;
    private Integer distance;
    public YamlConfiguration dataFile;
    public NPCManager nm;

    @Override
    public void onEnable() {
        loadConfig();
        loadDataFile();
        loadDeadPlayers();
        (new ALListener(this)).registerEvents();
        nm = new NPCManager(this);
        loadDataFile();
        getCommand("combat").setExecutor(new CommandExecutor() {

            public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
                if (!(cs instanceof Player)) {
                    cs.sendMessage(ChatColor.RED + "You cannot use this game from console!");
                    return false;
                }
                Player player = (Player) cs;
                if (inCombat(player.getName())) {
                    player.sendMessage(ChatColor.RED + "You're still in combat!");
                    return true;
                }
                player.sendMessage(ChatColor.YELLOW + "You're not in combat.");


                return true;
            }
        });
        LOGGER.log(Level.INFO, "[AntiPvPLogger] Enabled.");
    }

    @Override
    public void onDisable() {
        saveConfig();
        saveDataFile();
        saveDeadPlayers();
        LOGGER.log(Level.INFO, "[AntiPvPLogger] Disabled.");
    }

    public Boolean inCombat(String name) {
        if (combatMap.get(name) == null) {
            return false;
        }
        return combatMap.get(name);
    }

    public void setInCombat(String name, Boolean bool) {
        combatMap.put(name, bool);
    }

    public Boolean playersNearby(Player player) {
        for (Entity entity : player.getNearbyEntities(distance, distance, distance)) {
            if (!(entity instanceof Player)) {
                continue;
            }
            return true;
        }
        return false;
    }

    public void addDead(String name) {
        deadPlayers.add(name);
    }

    public void removeDead(String name) {
        deadPlayers.remove(name);
    }

    public Boolean isDead(String name) {
        return deadPlayers.contains(name);
    }

    public HumanNPC spawnHumanNPC(Player player, Location loc, String name) {
        HumanNPC npc = (HumanNPC) nm.spawnHumanNPC(name, loc);
        ItemStack[] invContents = player.getInventory().getContents();
        ItemStack[] armourContents = player.getInventory().getArmorContents();
        npc.getInventory().setContents(invContents);
        npc.getInventory().setArmorContents(armourContents);
        Bukkit.getScheduler().scheduleSyncDelayedTask(this, new DeSpawnTask(name, nm), time * 20L);
        return npc;
    }

    public void setTask(String name, Integer integer) {
        taskMap.put(name, integer);
    }

    public Integer getTask(String name) {
        if (taskMap.get(name) == null) {
            return -1;
        }
        return taskMap.get(name);
    }

    public void saveDeadPlayers() {
        LOGGER.log(Level.INFO, "[AntiPvPLogger] Saving " + deadPlayers.size() + " Dead Players.");
        getDataFile().set("deadPlayers", deadPlayers);
        saveDataFile();
        LOGGER.log(Level.INFO, "[AntiPvPLogger] Saving Complete.");

    }

    public void loadDeadPlayers() {
        if ((List<String>) getDataFile().getList("deadPlayers") == null) {
            LOGGER.log(Level.INFO, "[AntiPvPLogger] Could not load any Dead Players.");
            return;
        }
        deadPlayers = getDataFile().getStringList("deadPlayers");
        getDataFile().set("deadPlayers", null);
        saveDataFile();
        LOGGER.log(Level.INFO, "[AntiPvPLogger] Loaded " + deadPlayers.size() + " Dead Players.");
    }

    public YamlConfiguration loadDataFile() {
        File df = new File(getDataFolder().toString() + File.separator + "data.yml");

        if (!df.exists()) {
            try {
                df.createNewFile();
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, "Could not create the data file!", ex);
            }
        }

        this.dataFile = YamlConfiguration.loadConfiguration(df);
        return this.dataFile;
    }

    public YamlConfiguration getDataFile() {
        return dataFile;
    }

    public void saveDataFile() {
        File df = new File(getDataFolder().toString() + File.separator + "data.yml");
        try {
            this.dataFile.save(df);
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, "Could not save the data!", ex);
        }
    }

    public void loadConfig() {
        Configuration config = getConfig();
        config.addDefault("npc.spawn.distance", 10);
        config.set("npc.spawn.distance", config.getInt("npc.spawn.distance"));
        distance = config.getInt("npc.spawn.distance");
        config.addDefault("npc.spawn.time", 15);
        config.set("npc.spawn.distance", config.getInt("npc.spawn.time"));
        time = config.getInt("npc.spawn.time");
        getConfig().options().copyDefaults(true);
        this.saveConfig();
    }
}
